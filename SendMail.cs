﻿using Newtonsoft.Json;
using PCDB_Import.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace PCDB_Import
{
    public static class SendMail
    {
        //private static string BaseUrl;

        //public SendMail(IConfiguration configuration)
        //{
        //    BaseUrl = configuration["AppSettings:Recipient"];
        //}
        
        public static HttpWebResponse SendMailMessage(Mail m, string BaseUrl)
        {
            HttpWebResponse webResponse = null;
            string jo = JsonConvert.SerializeObject(m).ToString();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(BaseUrl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 200000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jo);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {

            }
            catch (Exception ex)
            {

            }
            return webResponse;
        }
    }
}