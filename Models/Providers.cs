﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PCDB_Import.Models
{
    class Providers
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EchoDoctorNumber { get; set; }
    }
}
