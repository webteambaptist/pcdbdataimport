﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblSmt
    {
        public int SmtidPk { get; set; }
        public string SmtfirstName { get; set; }
        public string Smtmi { get; set; }
        public string SmtlastName { get; set; }
        public string Smtsuffix { get; set; }
        public string Smtactive { get; set; }
    }
}
