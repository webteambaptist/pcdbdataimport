﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblSuffix
    {
        public string Suffix { get; set; }
    }
}
