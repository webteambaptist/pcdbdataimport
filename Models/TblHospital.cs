﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblHospital
    {
        public TblHospital()
        {
            TblRecipientsCourtesies = new HashSet<TblRecipientsCourtesy>();
        }

        public short HospitalIdPk { get; set; }
        public string HospitalName { get; set; }

        public virtual ICollection<TblRecipientsCourtesy> TblRecipientsCourtesies { get; set; }
    }
}
