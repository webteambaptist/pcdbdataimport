﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class VwRobinThomasHistory
    {
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public DateTime? DateGiven { get; set; }
        public string BhEntity { get; set; }
        public string GivenBy { get; set; }
        public string CourtesyType { get; set; }
        public decimal? Value { get; set; }
        public string Description { get; set; }
        public string LastModifiedUser { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
