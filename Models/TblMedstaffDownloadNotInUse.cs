﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblMedstaffDownloadNotInUse
    {
        public string Pas { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime? Dob { get; set; }
        public string Title { get; set; }
    }
}
