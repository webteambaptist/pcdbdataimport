﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblImportHistory
    {
        public short? Id { get; set; }
        public DateTime? LastImportDate { get; set; }
    }
}
