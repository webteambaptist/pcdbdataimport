﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblRecipient
    {
        public TblRecipient()
        {
            TblRecipientsCourtesies = new HashSet<TblRecipientsCourtesy>();
        }

        public string RecipientIdPk { get; set; }
        public string RecipientTitle { get; set; }
        public string RecipientFirstName { get; set; }
        public string RecipientMi { get; set; }
        public string RecipientLastName { get; set; }
        public string RecipientSuffix { get; set; }
        public DateTime? RecipientDob { get; set; }
        public string RecipientComments { get; set; }
        public string RecipientIdFk { get; set; }
        public short? RelationshipTypeIdFk { get; set; }
        public string LastModifiedUser { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? RecordCreatedDate { get; set; }
        public string Nspflag { get; set; }

        public virtual ICollection<TblRecipientsCourtesy> TblRecipientsCourtesies { get; set; }
    }
}
