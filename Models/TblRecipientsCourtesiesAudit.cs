﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PCDB_Import.Models
{
    public partial class TblRecipientsCourtesiesAudit
    {
        public int AuditIdPk { get; set; }
        public int RecipientCourtesyIdPk { get; set; }
        public string RecipientIdFk { get; set; }
        public short CourtesyTypeIdFk { get; set; }
        public short? HospitalIdFk { get; set; }
        public DateTime? DateGiven { get; set; }
        public decimal? ValueGiven { get; set; }
        public int? SmtidFk { get; set; }
        public string Description { get; set; }
        public string LastModifiedUser { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? RecordCreatedDate { get; set; }
        public string AuditType { get; set; }
        public DateTime? AuditTimeStamp { get; set; }
        public string Comments { get; set; }
    }
}
