﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using NLog;
using System.Net.Mail;
using PCDB_Import.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace PCDB_Import
{
    class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private const string NewLine = "<br />";

        private static string _to = ConfigurationManager.AppSettings["recipient"];
        private static string _from = ConfigurationManager.AppSettings["sender"];
        private static string _baseUrl = ConfigurationManager.AppSettings["mail"];

        static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();

            var file = new NLog.Targets.FileTarget("logfile") { FileName = $"PCDBImport-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, file);
            LogManager.Configuration = config;

            try
            {
                Console.WriteLine("Starting PCDB Import Process");
                Logger.Info("Starting PCDB Import Process");
                PCDBImport.ImportData();
                
                Environment.Exit(0);

            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + e.Message +
                             NewLine + " ex.InnerException: " + e.InnerException + NewLine);

                var m = new Mail()
                {
                    From = _from,
                    To = _to,
                    Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + e.Message +
                           NewLine + " ex.InnerException: " + e.InnerException + NewLine + "Full Exception: " + e,
                    Subject = "PCDB Import Process Failed"
                };

                var res = SendMail.SendMailMessage(m, _baseUrl);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Email sent successfully!");
                    Logger.Info("Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                Console.WriteLine(e.Message);
            }
        }
    }
}
