﻿using PCDB_Import.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;
using System.Linq;
using System.Net;
using NLog;

namespace PCDB_Import
{
    public static class PCDBImport
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private const string NewLine = "<br />";

        private static string _to = ConfigurationManager.AppSettings["recipient"];
        private static string _from = ConfigurationManager.AppSettings["sender"];
        private static string _baseUrl = ConfigurationManager.AppSettings["mail"];
        private static string _getProviders = ConfigurationManager.AppSettings["PCDB_URL"];

        private static List<Providers> _providers;

        public static void ImportData()
        {
            try
            {
                Console.WriteLine("Getting Providers for PCDB");
                Logger.Info("Getting Providers for PCDB");
                
                var m = new Mail()
                {
                    From = _from,
                    To = _to,
                    Body = "PCDB Import Started....",
                    Subject = "PCDB Import Started...."
                };

                var res = SendMail.SendMailMessage(m, _baseUrl);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Start Email sent successfully!");
                    Logger.Info("Start Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                _providers = GetProvidersFromImport();

                if (_providers == null)
                    return;
                bool isError = false;
                var providerError = "";
                try
                {
                    using (var context = new pcdbContext())
                    {
                        try
                        {
                            foreach (var provider in _providers)
                            {
                                try
                                {
                                    var id = provider.EchoDoctorNumber;

                                    var result = context.TblRecipients.FirstOrDefault(x => x.RecipientIdPk == id);

                                    // If there is already an existing record for this provider, simply update it.
                                    if (result != null)
                                    {
                                        result.RecipientTitle = "Dr.";
                                        result.RecipientFirstName = provider.FirstName;
                                        result.RecipientLastName = provider.LastName;
                                        result.LastModifiedUser = "sa";
                                        result.LastModifiedDate = DateTime.Now;
                                    }
                                    //If there is no record for this provider, insert it
                                    else
                                    {
                                        var newProvider = new TblRecipient
                                        {
                                            RecipientIdPk = provider.EchoDoctorNumber,
                                            RecipientTitle = "Dr.",
                                            RecipientFirstName = provider.FirstName,
                                            RecipientLastName = provider.LastName,
                                            LastModifiedUser = "sa",
                                            LastModifiedDate = DateTime.Now
                                        };
                                        context.TblRecipients.Add(newProvider);

                                        Logger.Info("New Provider Being Added " + provider.FirstName + " " + provider.LastName + "," + provider.EchoDoctorNumber);
                                    }

                                    context.SaveChanges();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                    Logger.Error("Error adding provider " + provider.FirstName + " " +
                                                        provider.LastName + " Error: " + e.Message);

                                    providerError += "Error adding provider " + provider.FirstName + " " +
                                                     provider.LastName + " Error: " + e.Message + NewLine;

                                    isError = true;
                                }
                            }

                            if (isError)
                            {

                                m = new Mail()
                                {
                                    From = _from,
                                    To = _to,
                                    Body = providerError,
                                    Subject = "PCDB Import Exception...."
                                };

                                res = SendMail.SendMailMessage(m, _baseUrl);
                                if (res.StatusCode == HttpStatusCode.OK)
                                {
                                    Console.WriteLine("Exception Email sent successfully!");
                                    Logger.Info("Exception Email sent successfully!");
                                }
                                else
                                {
                                    Console.WriteLine("Error sending Email");
                                    Logger.Info("Error sending Email");
                                }
                            }

                            //Update Import History
                            //var importHistory = context.TblImportHistories.FirstOrDefault();
                            //importHistory.LastImportDate = DateTime.Now;
                            //context.SaveChanges();
                        }
                        catch (Exception a)
                        {
                            Console.WriteLine(a);
                            Logger.Error("Exception occured before processing providers " + a.Message);

                            m = new Mail()
                            {
                                From = _from,
                                To = _to,
                                Body = "Exception occured before processing providers " + a.Message,
                                Subject = "PCDB Import Exception...."
                            };

                            res = SendMail.SendMailMessage(m, _baseUrl);
                            if (res.StatusCode == HttpStatusCode.OK)
                            {
                                Console.WriteLine("Exception Email sent successfully!");
                                Logger.Info("Exception Email sent successfully!");
                            }
                            else
                            {
                                Console.WriteLine("Error sending Email");
                                Logger.Info("Error sending Email");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Logger.Error("Exception from generated Provider List " + e.Message + " " + e.InnerException);

                    m = new Mail()
                    {
                        From = _from,
                        To = _to,
                        Body = "Exception from generated Provider List " + e.Message + " " + e.InnerException,
                        Subject = "PCDB Import Exception...."
                    };

                    res = SendMail.SendMailMessage(m, _baseUrl);
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Exception Email sent successfully!");
                        Logger.Info("Exception Email sent successfully!");
                    }
                    else
                    {
                        Console.WriteLine("Error sending Email");
                        Logger.Info("Error sending Email");
                    }
                }
                
                m = new Mail()
                {
                    From = _from,
                    To = _to,
                    Body = "PCDB Import Finished....",
                    Subject = "PCDB Import Finished...."
                };

                res = SendMail.SendMailMessage(m, _baseUrl);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Completion Email sent successfully!");
                    Logger.Info("Completion Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }

                Console.WriteLine("PCDB Import Completed!");
                Logger.Info("PCDB Import Completed!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
                Logger.Error("Exception occured PCDBImport.ImportData() " + e.Message + " " + e.InnerException);

                var m = new Mail()
                {
                    From = _from,
                    To = _to,
                    Body = "Exception occured PCDBImport.ImportData() " + e.Message + " " + e.InnerException,
                    Subject = "PCDB Import Exception...."
                };

                var res = SendMail.SendMailMessage(m, _baseUrl);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Exception Email sent successfully!");
                    Logger.Info("Exception Email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
            }
        }

        private static List<Providers> GetProvidersFromImport()
        {
            var providers = new List<Providers>();

            using (var results = new HttpClient())
            {
                var response = results.GetAsync(_getProviders).Result;
                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        providers = JsonConvert.DeserializeObject<List<Providers>>(response.Content.ReadAsStringAsync()
                            .Result);
                        return providers;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        Logger.Error("There was an issue getting Providers for the PCDB Import from the microservice." +
                                     NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                     ex.InnerException + NewLine);
                        return null;
                    }
                }
                else
                {
                    Logger.Error("There was an issue getting providers", response.RequestMessage);
                    return null;
                }
            }
        }
    }
}
